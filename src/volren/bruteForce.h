
#ifndef BRUTEFORCECUDA_H
#define BRUTEFORCECUDA_H

#include <algorithm>
#include "volumeReader.h"
#include "png_image.h"
#include <vector>
#include <queue>
#include <chrono>
#include "global_defines.h"
#include "../../config.h"
#include "CompressRBUC.h"

template <class A, class D, class O> 
void bruteForce_outer_caller(std::vector<CenterBlock<A, D, O>> &selected_blocks, std::vector<VolumeBlock<A, O>> &blocks, unsigned int k, unsigned int unique_blocks);


/* Combinations:

CenterBlock:

CenterBlock<unsigned long long, uint, uchar>
CenterBlock<unsigned long long, unsigned long long, unsigned short>
CenterBlock<unsigned long long, uint4, uchar4>
CenterBlock<unsigned long long, ulonglong4, ushort4>

VolumeBlock<unsigned long long, uchar>
VolumeBlock<unsigned long long, unsigned short>
VolumeBlock<unsigned long long, uchar4>
VolumeBlock<unsigned long long, ushort4>


*/
#endif /* BRUTEFORCECUDA_H */