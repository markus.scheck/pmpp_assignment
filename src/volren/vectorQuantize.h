#include "volumeReader.h"
#include <algorithm>
#include "png_image.h"
#include <vector>
#include <queue>
#include <chrono>
#include "global_defines.h"
#include "../../config.h"
#include "CompressRBUC.h"

class maxThreadDim{
public:
	unsigned int max_X;
	unsigned int max_Y;
	unsigned int max_Z;

	maxThreadDim(unsigned int iters_x1, unsigned int iters_y1,unsigned int iters_z1){
		max_X = iters_x1;
		max_Y = iters_y1;
		max_Z = iters_z1;
	};
};

template <class blocktype, class volumetype>
__global__ void psnr_kernel(blocktype* src_ray, volumetype vol_ray, float* dst_ray, cudaExtent volumeSize, unsigned int z0, unsigned int y0,unsigned int x0, maxThreadDim cur3Ddim);

/*
* param: selected_blocks IS used afterwards
* param: blocks IS used afterwards
* param: unique_blocks is NOT used afterwards
* param: bruteForce is not updated during function
* param: k is not updated during function
*/
//Das hier auf die GPU porten 
template <class T, class A, class D, class O>
void vector_quant_gpu_outer_caller(T *vol, cudaExtent &volumeSize,  std::vector<CenterBlock<A, D, O>>  &selected_blocks, std::vector<VolumeBlock<A, O>> &blocks, int unique_blocks, bool bruteForce, unsigned int k);


template <class blocktype, class volumetype, class hashblocktype> 
float calcPsnr_loc(volumetype *vol, blocktype &selectedBlocks, cudaExtent &volumeSize , unsigned int k, std::vector<hashblocktype> *hash_blocks);