// Copyright (c) 2016 Stefan Guthe / GCC / TU-Darmstadt. All rights reserved. 
// Use of this source code is governed by the BSD 3-Clause license that can be
// found in the LICENSE file.

#pragma once
#include "cuda_runtime.h"
#define _CRT_SECURE_NO_WARNINGS
#define NOMINMAX

#include <algorithm>
#include "png_image.h"
#include <vector>
#include <queue>
#include <chrono>
#include "global_defines.h"
#include "../../config.h"
#include "CompressRBUC.h"



typedef unsigned int  uint;
typedef unsigned char uchar;

#ifdef WIN32
#include <Windows.h>
#endif
#include <iostream>

#ifdef PVM_SUPPORT
#include "../v3/ddsbase.cpp"
#endif

#ifdef ZLIB_SUPPORT 
#include "zlib.h"
#endif

#ifdef LIBPNG_SUPPORT
#include "png_image.h"
#endif



#include <omp.h>


// included for writing ktx format
#include <GL/glew.h>

// arithmetic compression/decompression
#include "arithcoder.h"

typedef unsigned char VolumeType8;
typedef unsigned short VolumeType16;
typedef uchar4 VolumeType32;
typedef ushort4 VolumeType64;



template<class A, class D>
class VolumeBlock {
public:
	D value[64];
	unsigned short hash;
	unsigned int count;
	unsigned int last;

	A lastDist;
	A squared;
	double zeroDist;

	A dot(const D &a, const D &b);
	bool equal(const D &a, const D &b);
	void hashing(const D &a);
	bool operator==(VolumeBlock<A, D> &a);
	void calcHash();
	A dist(const VolumeBlock<A, D> &v);
	A dist();
	bool dist(VolumeBlock<A, D> &v, A min_dist, A &d);
};


template <class A, class D, class O>
class CenterBlock
{
public:
	omp_lock_t lock;
	O value[64];
	D sum[64];
	uint weight;
	bool changedCenter;
	bool changedMember;
	A squared;
	double zeroDist;

	CenterBlock();

	A dot(const O &a, const O &b);
	A dot2(const O &a, const D &b);
	void add(D &s, const O &v, const int c);
	void convert(O &v, const D &s);
	void assign(O &v, const D &s);

	A dist(VolumeBlock<A, O> &v);
	bool dist(VolumeBlock<A, O> &v, A &min_dist, bool eq);
	void add(const VolumeBlock<A, O> &v);
	void sub(const VolumeBlock<A, O> &v);
	void update();
	CenterBlock<A, D, O> &operator=(VolumeBlock<A, O> &a);

};
class MyClass {       // The class
  public:             // Access specifier
    int myNum;        // Attribute (int variable)
    unsigned char myString;  // Attribute (string variable)
};




#ifndef VOLUMEFILE_TEST_H
#define VOLUMEFILE_TEST_H
class VolumeFileTest;
#endif


#ifndef VOLUMEFILE_H
#define VOLUMEFILE_H
// file class to combine compressed (if enabled) and regular file access
class VolumeFile;
#endif


#ifndef WIN32
#define __forceinline inline __attribute__((always_inline))
#endif

// #ifndef VOLUMEBLOCK_H
// #define VOLUMEBLOCK_H
// template<class A, class D>
// class VolumeBlock;
// #endif


template <class T>
T *loadRawFile(char *filename, size_t size, float3 &scale, int raw_skip);

#ifdef LIBPNG_SUPPORT
template <class T>
T* loadPngFiles(char *filename, cudaExtent &volumeSize, float3 &scale, int start, int end, int clip_x0, int clip_x1, int clip_y0, int clip_y1, float scale_png, bool clip_zero);

unsigned int getPngElementSize(char *filename, int start);
unsigned int getPngComponents(char *filename, int start);
#endif

void *loadDatFile(char *filename, cudaExtent &volumeSize, float3 &scale, unsigned int &elementSize, unsigned int &components);

void saveDatFile(char *export_name, cudaExtent &volumeSize, float3 &scale, unsigned int &element_size, unsigned int &element_count, void *raw_volume, int volumeType, int export_version);

template <class T>
void denoiseVolume(T *vol, cudaExtent &volumeSize, int denoise);

template <class T, class A, class D, class O>
void quantizeVolume(T *vol, cudaExtent &volumeSize, int lossy, bool bruteForce);

template <class T>
void resampleVolume(T *vol, T *out, cudaExtent &volumeSize, cudaExtent &resampleSize);

template <class T>
void expandVolume(T *vol, T *out, cudaExtent &volumeSize, cudaExtent &resampleSize);

template <class T>
T *calculateGradients(T *vol, cudaExtent &volumeSize, unsigned int &components);

template <typename T>
void printSize(T *vol, cudaExtent volumeSize, unsigned int components);

float quantizeMaximum(unsigned char *vol);
float quantizeMaximum(unsigned short *vol);
float quantizeMaximum(uchar4 *vol);
float quantizeMaximum(ushort4 *vol);




template<>  unsigned long long VolumeBlock<unsigned long long, uchar>::dot(const uchar &a, const uchar &b);
template<>  unsigned long long VolumeBlock<unsigned long long, unsigned short>::dot(const unsigned short &a, const unsigned short &b);
template<>  unsigned long long VolumeBlock<unsigned long long, uchar4>::dot(const uchar4 &a, const uchar4 &b);
template<>  unsigned long long VolumeBlock<unsigned long long, ushort4>::dot(const ushort4 &a, const ushort4 &b); 

template<>  bool VolumeBlock<unsigned long long, uchar>::equal(const uchar &a, const uchar &b);
template<>  bool VolumeBlock<unsigned long long, unsigned short>::equal(const unsigned short &a, const unsigned short &b);
template<>  bool VolumeBlock<unsigned long long, uchar4>::equal(const uchar4 &a, const uchar4 &b);
template<>  bool VolumeBlock<unsigned long long, ushort4>::equal(const ushort4 &a, const ushort4 &b);

template<>  void VolumeBlock<unsigned long long, uchar>::hashing(const uchar &a);
template<>  void VolumeBlock<unsigned long long, unsigned short>::hashing(const unsigned short &a); 
template<>  void VolumeBlock<unsigned long long, uchar4>::hashing(const uchar4 &a);
template<>  void VolumeBlock<unsigned long long, ushort4>::hashing(const ushort4 &a);

template<>  unsigned long long CenterBlock<unsigned long long, uint, uchar>::dot2(const uchar &a, const uint &b);
template<>  unsigned long long CenterBlock<unsigned long long, unsigned long long, unsigned short>::dot2(const unsigned short &a, const unsigned long long &b) ;
template<>  unsigned long long CenterBlock<unsigned long long, uint4, uchar4>::dot2(const uchar4 &a, const uint4 &b);
template<>  unsigned long long CenterBlock<unsigned long long, ulonglong4, ushort4>::dot2(const ushort4 &a, const ulonglong4 &b);

template<>  unsigned long long CenterBlock<unsigned long long, uint, uchar>::dot(const uchar &a, const uchar &b);
template<>  unsigned long long CenterBlock<unsigned long long, unsigned long long, unsigned short>::dot(const unsigned short &a, const unsigned short &b);
template<>  unsigned long long CenterBlock<unsigned long long, uint4, uchar4>::dot(const uchar4 &a, const uchar4 &b);
template<>  unsigned long long CenterBlock<unsigned long long, ulonglong4, ushort4>::dot(const ushort4 &a, const ushort4 &b);

template<>  void CenterBlock<unsigned long long, uint, uchar>::add(uint &s, const uchar &v, const int c);
template<>  void CenterBlock<unsigned long long, unsigned long long, unsigned short>::add(unsigned long long &s, const unsigned short &v, const int c);
template<>  void CenterBlock<unsigned long long, uint4, uchar4>::add(uint4 &s, const uchar4 &v, const int c);
template<>  void CenterBlock<unsigned long long, ulonglong4, ushort4>::add(ulonglong4 &s, const ushort4 &v, const int c);

template<>  void CenterBlock<unsigned long long, uint, uchar>::convert(uchar &v, const uint &s);
template<>  void CenterBlock<unsigned long long, unsigned long long, unsigned short>::convert(unsigned short &v, const unsigned long long &s);
template<>  void CenterBlock<unsigned long long, uint4, uchar4>::convert(uchar4 &v, const uint4 &s);
template<>  void CenterBlock<unsigned long long, ulonglong4, ushort4>::convert(ushort4 &v, const ulonglong4 &s);

template<>  void CenterBlock<unsigned long long, uint, uchar>::assign(uchar &v, const uint &s);
template<>  void CenterBlock<unsigned long long, unsigned long long, unsigned short>::assign(unsigned short &v, const unsigned long long &s);
template<>  void CenterBlock<unsigned long long, uint4, uchar4>::assign(uchar4 &v, const uint4 &s);
template<>  void CenterBlock<unsigned long long, ulonglong4, ushort4>::assign(ushort4 &v, const ulonglong4 &s);


