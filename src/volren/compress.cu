#include <algorithm>
#include <iostream>
#include <stdint.h>

#define CUDA_CHECK_ERROR                                                       \
    do {                                                                       \
        const cudaError_t err = cudaGetLastError();                            \
        if (err != cudaSuccess) {                                              \
            const char *const err_str = cudaGetErrorString(err);               \
            std::cerr << "Cuda error in " << __FILE__ << ":" << __LINE__ - 1   \
                      << ": " << err_str << " (" << err << ")" << std::endl;   \
            exit(EXIT_FAILURE);                                                \
        }                                                                      \
    } while(0);

inline unsigned int div_up(unsigned int numerator, unsigned int denominator)
{
	unsigned int result = numerator / denominator;
	if (numerator % denominator) ++result;
	return result;
}

void get_grid_split(int width, int height, int depth, dim3& block, dim3& grid) {
	//get max. grid size
	cudaDeviceProp pr;
	int d;
	cudaGetDevice(&d);
	CUDA_CHECK_ERROR;
	cudaGetDeviceProperties(&pr, d);
	CUDA_CHECK_ERROR;
	
	//get block size limits
	int max_dim_x = pr.maxThreadsDim[0];
	int max_dim_y = pr.maxThreadsDim[1];
	int max_dim_z = pr.maxThreadsDim[2];
	int maxT = 8*8*8;//pr.maxThreadsPerBlock;

	//split calculating threads so that they fit block size
	int num_blocks_x = 1;
	int block_size_x = width;
	int num_blocks_y = 1;
	int block_size_y = height;
	int num_blocks_z = 1;
	int block_size_z = depth;

	while(block_size_x>max_dim_x) {
		block_size_x = div_up(width, ++num_blocks_x);
	}

	while(block_size_y>max_dim_y) {
		block_size_y = div_up(height, ++num_blocks_y);
	}

	while(block_size_z>max_dim_z) {
		block_size_z = div_up(height, ++num_blocks_z);
	}

	while(block_size_x*block_size_y*block_size_z>maxT) {
		block_size_x = div_up(width, ++num_blocks_x);
		block_size_y = div_up(height, ++num_blocks_y);
		block_size_z = div_up(height, ++num_blocks_z);
	}

    block.x = block_size_x;
    block.y = block_size_y;
	block.z = block_size_z;
    grid.x = num_blocks_x;
    grid.y = num_blocks_y;
	grid.z = num_blocks_z;

    std::cout << "Running " << num_blocks_x << ":" << num_blocks_y << " blocks of size " \
		<< block_size_x << ":" << block_size_y << std::endl;
}

template <typename T>
__device__ unsigned int compress8(T *in, unsigned char *comp)
{
	T max = T(0);
	for (unsigned int i = 0; i < 8; i++)
		if(in[i] > max) max = in[i];

	unsigned char bits = 0;
	while ((T(1) << bits) - T(1) < max) bits++;

	if (bits > 0u) {
		int idx = 0;
		int pos = 0;
		comp[0] = 0;
		for (int i = 0; i < 8; i++)
		{
			int rem = bits;
			comp[idx] |= (in[i] << pos);
			while (pos + rem >= 8)
			{
				idx++;
				rem -= (8 - pos);
				pos = 0;
				if (rem > 0) comp[idx] = (unsigned char)(in[i] >> (bits - rem));
				else comp[idx] = (unsigned char)0;
			}
			pos += rem;
		}
	}

	return bits;
}

template <typename T, typename T2>
__global__ void cmpr_gpu_kernel(cudaExtent volumeSize, char* raw, T* dst, int c, int mask) {
    //get cell to work on
    int tx = threadIdx.x;
	int ty = threadIdx.y;
    int tz = threadIdx.z;

	int bs_x = volumeSize.width/4;
	int bs_y = volumeSize.height/4;
    int bs_z = volumeSize.depth/4;

	int bid_x = blockIdx.x;
	int bid_y = blockIdx.y;
    int bid_z = blockIdx.z;

	//rm unnecessary threads
	if( \
		tx >= bs_x || \
		ty >= bs_y || \
		tz >= bs_z \
	) return;

    //resulting IDs
    int x = (bid_x*bs_x + tx)*4;
	int y = (bid_y*bs_y + ty)*4;
    int z = (bid_z*bs_z + tz)*4;

    //buffers
	unsigned int comp_bytes = 0;
    T2 tmp[64];
	int i = 0;
	T tmp_abs[64];
	unsigned char bits[8];
	T comp_buffer[128];
	unsigned char *comp_data = (unsigned char *)comp_buffer;
	unsigned char comp_bits[8];
	unsigned char bits_size;
	unsigned int off = 0;

	int offset = (x + volumeSize.width/4 * y + volumeSize.width/4*volumeSize.height*z) * (2 + 64);
	T* comp = &dst[offset+1];

    //get data locally
    for (int z0 = 0; z0 < 4; z0++) {
	    for (int y0 = 0; y0 < 4; y0++){
			for (int x0 = 0; x0 < 4; x0++){
				if ((x + x0 < volumeSize.width) && (y + y0 < volumeSize.height) && (z + z0 < volumeSize.depth)){
					tmp[i++] = ((T*)raw) [(x + x0) + ((y + y0) + ((z + z0) + (c * volumeSize.depth)) * volumeSize.height) * volumeSize.width] - ((mask + 1) >> 1);
				}
				else
				{
				    tmp[i++] = 0;
				}
            }
		}
	}

    //tmp2 = abs of tmp * 2
	for (int i = 0; i < 64; i++)
		if (tmp[i] < 0)
			tmp_abs[i] = (((T)(~tmp[i])) << 1u) + 1u;
		else
			tmp_abs[i] = ((T)tmp[i]) << 1u;


    
    for (unsigned int idx = 0; idx < 8; idx++){
		bits[idx] = compress8<T>(&(tmp_abs[idx * 8]), &(comp_data[off]));
		off += bits[idx];
	}
		bits_size = compress8<unsigned char>(bits, comp_bits);
		if (1 + bits_size + off < 1 + 64*sizeof(T)){
			comp[0] = bits_size;
			if (bits_size > 0)
				memcpy(&(comp[1]), comp_bits, bits_size);
			if (off > 0)
				memcpy(&(comp[1 + bits_size]), comp_data, off);
			comp_bytes =  1 + bits_size + off;
		}
		else{
			// fallback
			comp[0] = 0xff;
			memcpy(&(comp[1]), tmp_abs, 64*sizeof(T));
			comp_bytes =  1 + 64*sizeof(T);
		}
		dst[offset] = comp_bytes;
		

}

template <typename T, typename T2>
void cmpr_gpu(T **dst, size_t* dst_size, unsigned char* src, size_t src_size, cudaExtent volumeSize, int c, int mask) {
    dim3 block, grid;

	//copy source data over
	char *source_pointer; T *dest_pointer;
	cudaMalloc(&source_pointer, src_size);
	CUDA_CHECK_ERROR
	cudaMemcpy(source_pointer, src, src_size, cudaMemcpyHostToDevice);
	CUDA_CHECK_ERROR

	*dst_size = volumeSize.depth * volumeSize.height * volumeSize.width * (2 + 64);
	*dst = (T*)malloc(*dst_size);
	cudaMalloc(&dest_pointer, *dst_size);
	CUDA_CHECK_ERROR

	get_grid_split(volumeSize.width/4, volumeSize.height/4, volumeSize.depth/4, block, grid);

	//Kernel call
	cmpr_gpu_kernel<T,T2><<<grid,block>>>(volumeSize, source_pointer, dest_pointer, c, mask);
	CUDA_CHECK_ERROR
	std::cout << "dst_sz: " << *dst_size << std::endl;
	cudaMemcpy(*dst, dest_pointer, *dst_size, cudaMemcpyDeviceToHost);
	CUDA_CHECK_ERROR
	cudaFree(source_pointer);
	CUDA_CHECK_ERROR
	cudaFree(dest_pointer);
	CUDA_CHECK_ERROR
}


template void cmpr_gpu<unsigned char, char>(unsigned char **dst, size_t* dst_size, unsigned char* src, size_t src_size, cudaExtent volumeSize, int c, int mask);
//untested as code delivery causes segfault, thus not able to check result...
template void cmpr_gpu<unsigned short, short>(unsigned short **dst, size_t* dst_size, unsigned char* src, size_t src_size, cudaExtent volumeSize, int c, int mask);


