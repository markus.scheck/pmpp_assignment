#include "volumeReader.h"
#include <algorithm>
#include "png_image.h"
#include <vector>
#include <queue>
#include <chrono>
#include "global_defines.h"
#include "../../config.h"
#include "CompressRBUC.h"

#include "vectorQuantize.h"

//Needed for local functions that use omp
#include <omp.h>

// just for testing can be removed
#include <cxxabi.h>
#include <iostream>
#include <string>

//thrust library 
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/extrema.h>


#define CUDA_CHECK_ERROR                                                       \
    do {                                                                       \
        const cudaError_t err = cudaGetLastError();                            \
        if (err != cudaSuccess) {                                              \
            const char *const err_str = cudaGetErrorString(err);               \
            std::cerr << "Cuda error in " << __FILE__ << ":" << __LINE__ - 1   \
                      << ": " << err_str << " (" << err << ")" << std::endl;   \
            exit(EXIT_FAILURE);                                                \
        }                                                                      \
    } while(0);


//*****  local version of: sub(const VolumeBlock<A, O> &v)  ****************//
__host__ void loc_sub_host(CenterBlock<unsigned long long, uint, uchar> &center, const VolumeBlock<unsigned long long, uchar> &v){
	for (unsigned int i = 0; i < 64; i++)
	{
		//center.add(center.sum[i], v.value[i], -((int)v.count));
		center.sum[i] += (int)v.value[i] * -((int)v.count);
	}
	center.weight -= v.count;
};

//*****  local version of: sub(const VolumeBlock<A, O> &v)  ****************//

//*****  local version of: add(const VolumeBlock<A, O> &v)  ****************//
__host__ void loc_add_host(CenterBlock<unsigned long long, uint, uchar> &center, const VolumeBlock<unsigned long long, uchar> &v){
	for (unsigned int i = 0; i < 64; i++)
	{
		//center.add(center.sum[i], v.value[i], (int)v.count);
		center.sum[i] += (int)v.value[i] * ((int)v.count);
	}
	center.weight += v.count;
};
//*****  local version of: add(const VolumeBlock<A, O> &v)  ****************//

//*****  local version of: dist(VolumeBlock<unsigned long long, uchar> &v, unsigned long long  &min_dist, bool eq)  ****************//
 __host__ bool loc_dist_host(CenterBlock<unsigned long long, uint, uchar> center, VolumeBlock<unsigned long long, uchar> &v, unsigned long long  &min_dist, bool eq){
	typedef unsigned long long A;
	if ((center.zeroDist - v.zeroDist)*(center.zeroDist - v.zeroDist) > min_dist) return false;
	A dd = A(0);
	for (unsigned int i = 0; i < 64; i++)
	{
		dd += (unsigned long long)((unsigned int)center.value[i] * (unsigned int)v.value[i]); 
	}
	A d = center.squared + v.squared;
	d -= dd;
	d -= dd;
	if ((d < min_dist) || ((d == min_dist) && eq))
	{
		min_dist = d;
		return true;
	}
	return false;
}
//*****  local version of: dist(VolumeBlock<unsigned long long, uchar> &v, unsigned long long  &min_dist, bool eq)  ****************//

//*****  local version of: dist(VolumeBlock<unsigned long long, uchar> &v)  ****************//
__host__ unsigned long long loc_dist_host(CenterBlock<unsigned long long, uint, uchar> center, VolumeBlock<unsigned long long, uchar> &v){
	typedef unsigned long long A;	
	A dd = A(0);
	for (unsigned int i = 0; i < 64; i++)
	{
		dd += (unsigned long long)((unsigned int)center.value[i] * (unsigned int)v.value[i]); 
	}
	A d = center.squared + v.squared;
	d -= dd;
	d -= dd;
	return d;
};
//*****  local version of: dist(VolumeBlock<unsigned long long, uchar> &v)  ****************//




template<class volumetype,class singleBlocktype>
float psnr_caller(volumetype vol, singleBlocktype* selectedBlocks, cudaExtent &volumeSize, unsigned int c, unsigned int k,unsigned int iters_z1, unsigned int iters_y1,unsigned int iters_x1, unsigned int z0, unsigned int y0,unsigned int x0){


	//@TODO: Next step ->  copy the relevant parts (probably selected block to GPU memory)
	//@TODO: Then: Updatze Kernel

	int numThreads = iters_z1*iters_y1*iters_x1;


	//needed when matrix size exceeds block size
	const int max_threads_per_block = 1024;

	// sqrt(1024) = 32
	const int max_threads_per_block_x = 32;
	const int max_threads_per_block_y = 32;
	int num_blocks_x = 0;
	int num_blocks_y = 0;


	int cur_threads_per_block_x = 0;
	int cur_threads_per_block_y = 0;
	int cur_threads_per_block_z = 0;

	if(numThreads<=max_threads_per_block){
		cur_threads_per_block_x = iters_x1;
		cur_threads_per_block_y = iters_y1;
		cur_threads_per_block_z = iters_z1;
		num_blocks_x = 1;
		num_blocks_y = 1;
	}else{
		printf("\n\n\n\n\n\n\n\n\n\nSTILL TODO - Abborting!\n\n\n\n\n\n\n\n\n\n");
		return 0.0;
	};
	//printf("Entered 'psnr_caller' routine...\n");

	dim3 threadsPerBlock(cur_threads_per_block_x, cur_threads_per_block_y, cur_threads_per_block_z);
	dim3 numBlocks(num_blocks_x, num_blocks_y); 


	maxThreadDim cur3Ddim = maxThreadDim(iters_x1, iters_y1, iters_z1);
	//normally, shared mem should not be needed
	size_t sharedMemBytes = 64; 



	float psnr = 0.0;
	int el_count = 1;
	//int src_size = k*sizeof(singleBlocktype);

	//@TODO prototype, at the moment, we only have one block
	int src_size = sizeof(singleBlocktype);

	int volume_size = volumeSize.height * volumeSize.width * volumeSize.depth;//(cur3Ddim.max_X + (cur3Ddim.max_Y + cur3Ddim.max_Z * volumeSize.height) * volumeSize.width) * sizeof(volumetype);


	//printf("Size of volren: %d \n", volume_size);
	int dst_size = numThreads*sizeof(float);


	//We return one float per thread
	float results[numThreads];

	//copy source data over
	singleBlocktype* src_ray;

	float* dst_ray;

	volumetype vol_ray;

    // char * name = abi::__cxa_demangle(typeid(vol_ray).name(), 0, 0, 0);
    // std::cout << name << std::endl;
    // free(name);



	cudaMalloc(&src_ray, src_size);
	CUDA_CHECK_ERROR

	cudaMalloc(&vol_ray, volume_size);
	CUDA_CHECK_ERROR


	//@TODO prototype, at the moment, we only have one block
	singleBlocktype testBlock = selectedBlocks[c];
	cudaMemcpy(src_ray, (void*) &testBlock, src_size, cudaMemcpyHostToDevice);
	CUDA_CHECK_ERROR

	
	cudaMemcpy(vol_ray, (void*) vol, volume_size, cudaMemcpyHostToDevice);
	CUDA_CHECK_ERROR


	cudaMalloc(&dst_ray, dst_size);
	CUDA_CHECK_ERROR


	//uchar val = selectedBlocks.value[v];
	// int block = 1;
	
	psnr_kernel<singleBlocktype,volumetype><<<numBlocks, threadsPerBlock, sharedMemBytes>>>(src_ray,vol_ray,dst_ray,volumeSize, z0, y0, x0, cur3Ddim);
	CUDA_CHECK_ERROR



	cudaMemcpy(results, dst_ray, dst_size, cudaMemcpyDeviceToHost);
	CUDA_CHECK_ERROR
	
	cudaMemcpy((void*) vol , vol_ray, volume_size, cudaMemcpyDeviceToHost);
	CUDA_CHECK_ERROR

	for (int i = 0; i<numThreads; i++){
		psnr += results[i];
		//printf("PSNR %d: %d\n", i, results[i]);
	}
	

	CUDA_CHECK_ERROR
	cudaFree(src_ray);
	CUDA_CHECK_ERROR

	cudaFree(vol_ray);
	CUDA_CHECK_ERROR

	cudaFree(dst_ray);
	CUDA_CHECK_ERROR

	return psnr;
};




// Matrix multiplication kernel – thread specification
template <class blocktype, class volumetype>
__global__ void psnr_kernel(blocktype* src_ray, volumetype vol_ray, float* dst_ray, cudaExtent volumeSize, unsigned int z0, unsigned int y0,unsigned int x0, maxThreadDim cur3Ddim){
	//CenterBlock<unsigned long long, uint, uchar> B;
    int tx = threadIdx.x;
	int ty = threadIdx.y;
    int tz = threadIdx.z;
	// int bs_x = blockDim.x;
	// int bs_y = blockDim.y;
    // int bs_z = blockDim.z;
	// int bid_x = blockIdx.x;
	// int bid_y = blockIdx.y;
    // int bid_z = blockIdx.z;
	unsigned int z1 = tz;
	unsigned int y1 = ty;
	unsigned int x1 = tx;

	unsigned int z = z0 + z1;
	unsigned int y = y0 + y1;
	unsigned int x = x0 + x1;
	

	unsigned int volIndex = x + (y + z * volumeSize.height) * volumeSize.width;

	float psnr = 0.0f;
	int v = x1 + y1 * 4 + z1 * 16;

	//printf("Chad GPU v: %d\n", v);

	//Prototype, we have only one block 
	blocktype cur_Block = src_ray[0];
	uchar val = cur_Block.value[v];

	//printf("Chad GPU Volume Index: %d\n", volIndex);
	//Prototype, we have only one block 
	unsigned char cur_vol = vol_ray[volIndex];

	psnr = (float)(((int)cur_vol - (int)val) * ((int)cur_vol - (int)val));



	//printf("Chad GPU psnr: %f\n", psnr);
	vol_ray[volIndex] = (unsigned char)val;
	dst_ray[x1 + (y1*cur3Ddim.max_X) + (z1*cur3Ddim.max_Y*cur3Ddim.max_X)] = psnr;
};

//++++++++++++++++++++++++++++++++BACKUP TEMPLATE++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
//	psnr_kernel<bool><<<1,block>>>(inp1, inp2, el_count,src_ray, dst_ray);
//++++++++++++++++++++++++++++++++BACKUP TEMPLATE++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// // Matrix multiplication kernel – thread specification
// template<class T>
// __global__ void psnr_kernel(const int inp1, const int inp2, int el_count,const int* src, int* dst){
// 	int b = blockIdx.x*blockDim.x + threadIdx.x;
	
// 	for(int i = 0; i<4; i++){
// 		dst[i] = inp2*(src[i]+inp1+b);
// 	};
// };
//++++++++++++++++++++++++++++++++BACKUP TEMPLATE++++++++++++++++++++++++++++++++++++++++++++++++++++++++//




unsigned long long dot_loc(const uchar &a, const uchar &b) { return (unsigned long long)((unsigned int)a * (unsigned int)b); }
unsigned long long dot_loc(const unsigned short &a, const unsigned short &b) { return (unsigned long long)((unsigned int)a * (unsigned int)b); }
unsigned long long dot_loc(const uchar4 &a, const uchar4 &b) { return (unsigned long long)((unsigned int)a.x * (unsigned int)b.x + (unsigned int)a.y * (unsigned int)b.y + (unsigned int)a.z * (unsigned int)b.z + (unsigned int)a.w * (unsigned int)b.w); }
unsigned long long dot_loc(const ushort4 &a, const ushort4 &b) { return (unsigned long long)((unsigned int)a.x * (unsigned int)b.x + (unsigned int)a.y * (unsigned int)b.y + (unsigned int)a.z * (unsigned int)b.z + (unsigned int)a.w * (unsigned int)b.w); }

/*
* param: selected_blocks IS used afterwards
* param: blocks IS used afterwards
* param: unique_blocks is NOT used afterwards
* param: bruteForce is not updated during function
* param: k is not updated during function
* 		 k amount of elements in selectedBlocks 
*/
//Das hier auf die GPU porten 
template <class blocktype, class volumetype, class hashblocktype> 
float calcPsnr_loc(volumetype *vol, blocktype &selectedBlocks, cudaExtent &volumeSize , unsigned int k, std::vector<hashblocktype> *hash_blocks);



template <> float calcPsnr_loc<std::vector<CenterBlock<unsigned long long, uint, uchar>>, unsigned char, VolumeBlock<unsigned long long, uchar>>
(unsigned char *vol, std::vector<CenterBlock<unsigned long long, uint, uchar>> &selectedBlocks, cudaExtent &volumeSize,  unsigned int k,std::vector<VolumeBlock<unsigned long long, uchar>>*hash_blocks)
{
	//printf("Entered 'calcPsnr_loc' routine...");
	long int counter_loop_z0 = 0;
	long int counter_loop_y0 = 0;
	long int counter_loop_x0 = 0;
	long int counter_loop_z1 = 0;
	long int counter_loop_y1 = 0;
	long int counter_loop_x1 = 0;
	long int counter_loop_internal = 0;



	// The amount of iterations is the product of all three dimensions
	float psnr = 0;

	for (unsigned int z0 = 0; z0 < volumeSize.depth; z0 += 4)
	{
		for (unsigned int y0 = 0; y0 < volumeSize.height; y0 += 4)
		{
			for (unsigned int x0 = 0; x0 < volumeSize.width; x0 += 4)
			{
				VolumeBlock<unsigned long long, uchar> tmp;
				for (unsigned int z1 = 0; z1 < 4; z1++)
				{
					unsigned int z = std::min((unsigned int)(volumeSize.depth - 1), z0 + z1);
					for (unsigned int y1 = 0; y1 < 4; y1++)
					{
						unsigned int y = std::min((unsigned int)(volumeSize.height - 1), y0 + y1);
						for (unsigned int x1 = 0; x1 < 4; x1++)
						{
							unsigned int x = std::min((unsigned int)(volumeSize.width - 1), x0 + x1);
							//tmp.value[x1 + y1 * 4 + z1 * 16] = convert(vol[x + (y + z * volumeSize.height) * volumeSize.width]);
							tmp.value[x1 + y1 * 4 + z1 * 16] = vol[x + (y + z * volumeSize.height) * volumeSize.width];

						}
					}
				}
				tmp.calcHash();
				unsigned int c = 0;
				for (unsigned int i = 0; i < hash_blocks[tmp.hash].size(); i++)
				{
					if (tmp == hash_blocks[tmp.hash][i]) c = hash_blocks[tmp.hash][i].last;
				}
					//iter indicates the amount of elements
				int iters_z1 = 0;
				if(volumeSize.depth - z0 < 4){
					iters_z1 = volumeSize.depth - z0;
				}else{
					iters_z1 = 4;	
				}

				int iters_y1 = 0;
				if(volumeSize.height - y0 < 4){
					iters_y1 = volumeSize.depth - y0;
				}else{
					iters_y1 = 4;	
				}

				int iters_x1 = 0;
				if(volumeSize.width - x0 < 4){
					iters_x1 = volumeSize.width - x0;
				}else{
					iters_x1 = 4;	
				}
					//Convert std::vector<...> to normal array (CUDA does not support std::vector lib)
 				CenterBlock<unsigned long long, uint, uchar> *selectedBlocks_ray = &selectedBlocks[0];
				psnr += psnr_caller<unsigned char*, CenterBlock<unsigned long long, uint, uchar>>(vol, selectedBlocks_ray, volumeSize, c, k, iters_z1, iters_y1, iters_x1, z0, y0, x0);

			}
		}
	}



	printf("counter_loop_internal: %li\n", counter_loop_internal);

	
	// char print_counters	= 0;
	// if(print_counters = 1){
	// 	printf("z0 counter: %li\n", counter_loop_z0);
	// 	printf("y0 counter: %li\n", counter_loop_y0);
	// 	printf("x0 counter: %li\n", counter_loop_x0);

	// 	printf("z1 counter: %li\n", counter_loop_z1);
	// 	printf("y1 counter: %li\n", counter_loop_y1);
	// 	printf("x1 counter: %li\n", counter_loop_x1);
	// }
	return psnr;
}

template <> float calcPsnr_loc<std::vector<CenterBlock<unsigned long long, unsigned long long, unsigned short>>, unsigned short>
(unsigned short *vol, std::vector<CenterBlock<unsigned long long, unsigned long long, unsigned short>> &sel, cudaExtent &volumeSize,  
unsigned int k, std::vector<VolumeBlock<unsigned long long, unsigned short>>*hash_blocks)
{
	printf("GuMo 2");
	float psnr = 0.0f;
	// unsigned short val = sel.value[v];
	// psnr = (float)(((int)vol - (int)val) * ((int)vol - (int)val));
	// vol = val;
	return psnr;
}

template <> float calcPsnr_loc<std::vector<CenterBlock<unsigned long long, uint4, uchar4>>, uchar4>
(uchar4 *vol, std::vector<CenterBlock<unsigned long long, uint4, uchar4>> &sel, cudaExtent &volumeSize,  unsigned int k,std::vector<VolumeBlock<unsigned long long, uchar4>>*hash_blocks)
{
	printf("GuMo 3");
	float psnr = 0.0f;
	// uchar4 val = sel.value[v];
	// psnr  = (float)(((int)vol.x - (int)val.x) * ((int)vol.x - (int)val.x));
	// psnr += (float)(((int)vol.y - (int)val.y) * ((int)vol.y - (int)val.y));
	// psnr += (float)(((int)vol.z - (int)val.z) * ((int)vol.z - (int)val.z));
	// psnr += (float)(((int)vol.w - (int)val.w) * ((int)vol.w - (int)val.w));
	// vol = val;
	return psnr;
}

template <> float calcPsnr_loc<std::vector<CenterBlock<unsigned long long, ulonglong4, ushort4>>, ushort4>
(ushort4 *vol, std::vector<CenterBlock<unsigned long long, ulonglong4, ushort4>> &sel, cudaExtent &volumeSize, unsigned int k, std::vector<VolumeBlock<unsigned long long, ushort4>>*hash_blocks)
{
	printf("GuMo 4");
	float psnr = 0.0f;
	// ushort4 val = sel.value[v];
	// psnr  = (float)(((int)vol.x - (int)val.x) * ((int)vol.x - (int)val.x));
	// psnr += (float)(((int)vol.y - (int)val.y) * ((int)vol.y - (int)val.y));
	// psnr += (float)(((int)vol.z - (int)val.z) * ((int)vol.z - (int)val.z));
	// psnr += (float)(((int)vol.w - (int)val.w) * ((int)vol.w - (int)val.w));
	// vol = val;
	return psnr;
}




//*****  local version of: sub(const VolumeBlock<A, O> &v)  ****************//
__device__ void loc_sub_dev(CenterBlock<unsigned long long, uint, uchar> &center, const VolumeBlock<unsigned long long, uchar> &v){
	for (unsigned int i = 0; i < 64; i++)
	{
		//center.add(center.sum[i], v.value[i], -((int)v.count));
		uint temp = (int)v.value[i] * -((int)v.count);
		atomicAdd(&(center.sum[i]), temp);
	}
	atomicSub(&(center.weight), v.count);
	//center.weight -= v.count;
};

//*****  local version of: sub(const VolumeBlock<A, O> &v)  ****************//

//*****  local version of: add(const VolumeBlock<A, O> &v)  ****************//
__device__ void loc_add_dev(CenterBlock<unsigned long long, uint, uchar> &center, const VolumeBlock<unsigned long long, uchar> &v){
	for (unsigned int i = 0; i < 64; i++)
	{
		//center.add(center.sum[i], v.value[i], (int)v.count);
		uint temp = (int)v.value[i] * ((int)v.count);
		atomicAdd(&(center.sum[i]), temp);
	}

	atomicAdd(&(center.weight), v.count);

	//center.weight += v.count;
};
//*****  local version of: add(const VolumeBlock<A, O> &v)  ****************//

//*****  local version of: dist(VolumeBlock<unsigned long long, uchar> &v, unsigned long long  &min_dist, bool eq)  ****************//
 __device__ bool loc_dist_dev(CenterBlock<unsigned long long, uint, uchar> center, VolumeBlock<unsigned long long, uchar> &v, unsigned long long  &min_dist, bool eq){
	typedef unsigned long long A;
	if ((center.zeroDist - v.zeroDist)*(center.zeroDist - v.zeroDist) > min_dist) return false;
	A dd = A(0);
	for (unsigned int i = 0; i < 64; i++)
	{
		dd += (unsigned long long)((unsigned int)center.value[i] * (unsigned int)v.value[i]); 
	}
	A d = center.squared + v.squared;
	d -= dd;
	d -= dd;
	if ((d < min_dist) || ((d == min_dist) && eq))
	{
		min_dist = d;
		return true;
	}
	return false;
}
//*****  local version of: dist(VolumeBlock<unsigned long long, uchar> &v, unsigned long long  &min_dist, bool eq)  ****************//

//*****  local version of: dist(VolumeBlock<unsigned long long, uchar> &v)  ****************//
__device__ unsigned long long loc_dist_dev(CenterBlock<unsigned long long, uint, uchar> center, VolumeBlock<unsigned long long, uchar> &v){
	typedef unsigned long long A;	
	A dd = A(0);
	for (unsigned int i = 0; i < 64; i++)
	{
		dd += (unsigned long long)((unsigned int)center.value[i] * (unsigned int)v.value[i]); 
	}
	A d = center.squared + v.squared;
	d -= dd;
	d -= dd;
	return d;
};
//*****  local version of: dist(VolumeBlock<unsigned long long, uchar> &v)  ****************//




 __global__ void quant_kernel(CenterBlock<unsigned long long, uint, uchar>* selected_blocks, VolumeBlock<unsigned long long, uchar>* blocks, 
int unique_blocks, bool bruteForce, unsigned int k, int run, unsigned int* changed_blocks, bool* switched_ray, double* psnr_dlt){

    int tx = threadIdx.x + (blockIdx.x*blockDim.x);
    if(tx>=unique_blocks){
        return;
	}

	typedef unsigned long long A;
	typedef  uint D;
	typedef  uchar O;
	int idx = tx;
	// for (int idx = 0; idx < (int)unique_blocks; idx++)
	// {
		VolumeBlock<A, O> &blk = blocks[idx];
		CenterBlock<A, D, O> &center = selected_blocks[blk.last];
		unsigned int c = blk.last;
		unsigned int test = blk.last;
		A min_dist = A(0);
		if ((run == 0) && (bruteForce))
		{
			min_dist = blk.lastDist;
		}
		else
		{
			if (center.changedCenter)
			{
				
				min_dist = loc_dist_dev(center,blk);
			}
			else
				min_dist = blk.lastDist;
			if ((center.changedCenter) && (min_dist > blk.lastDist))
			{
				// increased distance so need to check all
				for (unsigned int i = 0; i < k; i++)
				{
					if ((min_dist == 0) && (i > c)) break;
					if (i != blk.last)
					{
						CenterBlock<A, D, O> &comp = selected_blocks[i];
						//printf("\n\n\n\n IN KERNEL loc_dist_de if\n\n\n\n ");
						if (loc_dist_dev(comp,blk, min_dist, (i < c)))
						{

							c = i;
							test = i;
						}
					}
				}
			}
			else
			{
				// distance decreased or unchanged, only check modified blocks
				for (unsigned int ii = 0; ii < k; ii++)
				{
					unsigned int i = changed_blocks[ii];
					if ((min_dist == 0) && (i > c)) break;
					if (i != blk.last)
					{
						CenterBlock<A, D, O> &comp = selected_blocks[i];
						if (loc_dist_dev(comp,blk, min_dist, (i < c)))
						{
							c = i;
						}
					}
				}

			}
		}

		CenterBlock<A, D, O> &updated = selected_blocks[c];
	 	if ((blk.last != c) || (run == 0)){
	 		switched_ray[idx] = 1;
			if (run > 0)
			{
				loc_sub_dev(center,blk);
			}

			loc_add_dev(updated, blk);
			center.changedMember = true;
			updated.changedMember = true;
			blk.last = c;
	 	}
		else{
			switched_ray[idx] = 0;
		}
		{
			double add = 0.0;
			//if (run > 0) add -= (double)blk.lastDist * (double)blk.count;
			add += (double)min_dist * (double)blk.count;
			psnr_dlt[idx] = add;
	}
		blk.lastDist = min_dist;
	// }
}











//*************************************************************************//
//**********************  Forward Declarations ****************************//
template <typename T, typename A, typename D, typename O>
void vector_quant_gpu_outer_caller(T *vol, cudaExtent &volumeSize, std::vector<CenterBlock<A, D, O>>  &selected_blocks, std::vector<VolumeBlock<A, O>> &blocks, int unique_blocks, bool bruteForce, unsigned int k);
//**********************  Forward Declarations ****************************//
//*************************************************************************//


void ext_func(std::vector<CenterBlock<unsigned long long, uint, uchar>> &selected_blocks,std::vector<VolumeBlock<unsigned long long, uchar>> &blocks, 
int unique_blocks, bool bruteForce, unsigned int k, int run, unsigned int &changed_blocks_size, unsigned int *changed_blocks, unsigned int &switched, double &psnr_tmp, double &psnr_dlt){

typedef unsigned long long A;
typedef  uint D;
typedef  uchar O;

/********************HIER PARALELLIESIEREN!**************************/
#pragma omp parallel for reduction(+: psnr_dlt, switched)
		for (int idx = 0; idx < (int)unique_blocks; idx++)
		{
			VolumeBlock<A, O> &blk = blocks[idx];
			CenterBlock<A, D, O> &center = selected_blocks[blk.last];
			unsigned int c = blk.last;
			A min_dist = A(0);

			if ((run == 0) && (bruteForce))
			{
				min_dist = blk.lastDist;
			}
			else
			{
				if (center.changedCenter)
				{
					min_dist = center.dist(blk);
				}
				else
					min_dist = blk.lastDist;

				if ((center.changedCenter) && (min_dist > blk.lastDist))
				{
					// increased distance so need to check all
					for (unsigned int i = 0; i < k; i++)
					{
						if ((min_dist == 0) && (i > c)) break;
						if (i != blk.last)
						{
							CenterBlock<A, D, O> &comp = selected_blocks[i];
							if (comp.dist(blk, min_dist, (i < c)))
							{
								c = i;
							}
						}
					}
				}
				else
				{
					// distance decreased or unchanged, only check modified blocks
					for (unsigned int ii = 0; ii < changed_blocks_size; ii++)
					{
						unsigned int i = changed_blocks[ii];
						if ((min_dist == 0) && (i > c)) break;
						if (i != blk.last)
						{
							CenterBlock<A, D, O> &comp = selected_blocks[i];
							if (comp.dist(blk, min_dist, (i < c)))
							{
								c = i;
							}
						}
					}
				}
			}
			CenterBlock<A, D, O> &updated = selected_blocks[c];

			if ((blk.last != c) || (run == 0))
			{
				switched++;
				if (run > 0)
				{
					//loc_sub(center,blk);
					center.sub(blk);
				}

				updated.add(blk);
				center.changedMember = true;
				updated.changedMember = true;
				blk.last = c;
			}
			{
				double add = 0.0;
				//if (run > 0) add -= (double)blk.lastDist * (double)blk.count;
				add += (double)min_dist * (double)blk.count;
				psnr_dlt += add;
			}
			blk.lastDist = min_dist;
		}
		psnr_tmp = psnr_dlt;
		changed_blocks_size = 0;
/********************HIER PARALELLIESIEREN!**************************/
}



void ext_func_modified(CenterBlock<unsigned long long, uint, uchar>* selected_blocks, VolumeBlock<unsigned long long, uchar>* blocks, 
int unique_blocks, bool bruteForce, unsigned int k, int run, unsigned int* changed_blocks, bool* switched_ray, double* psnr_dlt){

	typedef unsigned long long A;
	typedef  uint D;
	typedef  uchar O;

	for (int idx = 0; idx < (int)unique_blocks; idx++)
	{
		VolumeBlock<A, O> &blk = blocks[idx];
		CenterBlock<A, D, O> &center = selected_blocks[blk.last];
		unsigned int c = blk.last;
		A min_dist = A(0);
		if ((run == 0) && (bruteForce))
		{
			min_dist = blk.lastDist;
		}
		else
		{
			if (center.changedCenter)
			{
				
				min_dist = loc_dist_host(center,blk);
			}
			else
				min_dist = blk.lastDist;
			if ((center.changedCenter) && (min_dist > blk.lastDist))
			{
				// increased distance so need to check all
				for (unsigned int i = 0; i < k; i++)
				{
					if ((min_dist == 0) && (i > c)) break;
					if (i != blk.last)
					{
						CenterBlock<A, D, O> &comp = selected_blocks[i];
						if (loc_dist_host(comp,blk, min_dist, (i < c)))
						{
							c = i;
						}
					}
				}
			}
			else
			{
				// distance decreased or unchanged, only check modified blocks
				for (unsigned int ii = 0; ii < k; ii++)
				{
					unsigned int i = changed_blocks[ii];
					if ((min_dist == 0) && (i > c)) break;
					if (i != blk.last)
					{
						CenterBlock<A, D, O> &comp = selected_blocks[i];
						if (loc_dist_host(comp,blk, min_dist, (i < c)))
						{
							c = i;
						}
					}
				}
			}
		}

		CenterBlock<A, D, O> &updated = selected_blocks[c];
		if ((blk.last != c) || (run == 0))
		{
			switched_ray[idx] = 1;
			if (run > 0)
			{
				loc_sub_host(center,blk);
			}

			loc_add_host(updated, blk);
			center.changedMember = true;
			updated.changedMember = true;
			blk.last = c;
		}else{
			switched_ray[idx] = 0;
		}
		{
			double add = 0.0;
			//if (run > 0) add -= (double)blk.lastDist * (double)blk.count;
			add += (double)min_dist * (double)blk.count;
			psnr_dlt[idx] = add;
		}
		blk.lastDist = min_dist;
	}
}



template <> void vector_quant_gpu_outer_caller<unsigned char, unsigned long long, uint, uchar>
(unsigned char *vol,cudaExtent &volumeSize, std::vector<CenterBlock<unsigned long long, uint, uchar>> &selected_blocks,std::vector<VolumeBlock<unsigned long long, uchar>> &blocks, int unique_blocks, bool bruteForce, unsigned int k){
	typedef unsigned char T;
	typedef unsigned long long A;
	typedef  uint D;
	typedef  uchar O;

	unsigned int *changed_blocks = new unsigned int[k];
	for (unsigned int i = 0; i < k; i++){
		changed_blocks[i] = i;
	}
	unsigned int changed_blocks_size = k;

	unsigned int changed = changed_blocks_size;

	int run = 0;

	double psnr_tmp = 0.0;

	while ((changed > 0) && (run < 1000))
	{
		changed = 0;
		unsigned int switched = 0;
		double psnr_dlt = 0.0;

		//convert center blocks vector to normal array 
        CenterBlock<A, D, O>* sel_ray = &selected_blocks[0];
    	VolumeBlock<A, O>* block_ray = &blocks[0];

        //inititalize pointer for device
        CenterBlock<A, D, O>* sel_blocks_dev;
		VolumeBlock<A, O>* blocks_dev;
		unsigned int* changed_blocks_dev;


        //allocate space for the volume blocks
        int size_sel_blocks = unique_blocks*sizeof(CenterBlock<A, D, O>);
		int size_blocks = unique_blocks*sizeof(VolumeBlock<A, O>);
		int size_changed_blocks = k*sizeof(unsigned int);

        cudaMalloc((void **) &sel_blocks_dev, size_sel_blocks);
		cudaMalloc((void **) &blocks_dev, size_blocks);
		cudaMalloc((void **) &changed_blocks_dev, size_changed_blocks);
        CUDA_CHECK_ERROR

        //inititalize pointer for device (Reduction arrays)
        bool* switched_ray_dev;
		double* psnr_dlt_ray_dev;

		//Allocate space for counters that are reduced later
		int size_switched_ray = unique_blocks*sizeof(bool);
		int size_psnr_dlt_ray = unique_blocks*sizeof(double);
        cudaMalloc((void **) &switched_ray_dev, size_switched_ray);
		cudaMalloc((void **) &psnr_dlt_ray_dev, size_psnr_dlt_ray);
        CUDA_CHECK_ERROR

        //copy the volume blocks to the device
        cudaMemcpy(sel_blocks_dev , sel_ray, size_sel_blocks, cudaMemcpyHostToDevice);
		cudaMemcpy(blocks_dev , block_ray, size_blocks, cudaMemcpyHostToDevice);
		cudaMemcpy(changed_blocks_dev , changed_blocks, size_changed_blocks, cudaMemcpyHostToDevice);
		CUDA_CHECK_ERROR

		//These arrays are used for later reduction 
		double psnr_dlt_ray[unique_blocks];
		bool switched_ray[unique_blocks];


		/*********************** CUDA Kernel!*****************************/
        const int max_threads_per_block = 1024;

        // int num_blocks = 1;
		// int cur_threads_per_block = 1;

        int num_blocks = 0;
        int cur_threads_per_block = 0;

        if(unique_blocks<=max_threads_per_block){
            cur_threads_per_block = unique_blocks;
            num_blocks = 1;

        }else{
            cur_threads_per_block = max_threads_per_block;
            num_blocks = unique_blocks / max_threads_per_block;

	        if (unique_blocks % max_threads_per_block){
                ++num_blocks;
            };
        };

		
        dim3 threadsPerBlock(cur_threads_per_block);
        dim3 numBlocks(num_blocks); 

		printf("\nUnique number of Blocks: %d\n",unique_blocks);
		printf("Threads per Block: %d\n",cur_threads_per_block);
		printf("Number of Block: %d\n",num_blocks);

		quant_kernel<<<numBlocks, threadsPerBlock>>>(sel_blocks_dev, blocks_dev, unique_blocks, bruteForce, k, run, changed_blocks_dev, switched_ray_dev, psnr_dlt_ray_dev);
		CUDA_CHECK_ERROR



		CenterBlock<A, D, O>* sel_block = (CenterBlock<A, D, O>*) malloc(1 * sizeof(CenterBlock<A, D, O>));
		
		// Copy data back to device
		cudaMemcpy(sel_ray, sel_blocks_dev, size_sel_blocks, cudaMemcpyDeviceToHost);
		CUDA_CHECK_ERROR
		cudaMemcpy(block_ray, blocks_dev, size_blocks, cudaMemcpyDeviceToHost);
		CUDA_CHECK_ERROR




		//reduce array with switched values
		thrust::device_ptr<bool> switched_thrust_ptr = thrust::device_pointer_cast(switched_ray_dev);
		switched =  thrust::reduce(switched_thrust_ptr, switched_thrust_ptr+unique_blocks, (int) 0, thrust::plus<int>());

		//reduce array with psnr_temp values
		thrust::device_ptr<double> psnr_thrust_ptr = thrust::device_pointer_cast(psnr_dlt_ray_dev);
		psnr_tmp =  thrust::reduce(psnr_thrust_ptr, psnr_thrust_ptr+unique_blocks, (int) 0, thrust::plus<int>());

		changed_blocks_size = 0;

		cudaFree(sel_blocks_dev);
		cudaFree(blocks_dev);
		cudaFree(switched_ray_dev);
		cudaFree(psnr_dlt_ray_dev);
		cudaFree(changed_blocks_dev);
		CUDA_CHECK_ERROR
		/*********************** CUDA Kernel!*****************************/


		/******************** Modified function!**************************/
		// switched = 0;
		// psnr_dlt = 0.0;
		// printf("CHECKPOINT 5\n");
		// ext_func_modified(sel_ray, block_ray, unique_blocks, bruteForce, k, run, changed_blocks, switched_ray, psnr_dlt_ray);	
		// printf("CHECKPOINT 6\n");
		// changed_blocks_size = 0;
		// //Sum up psnr_tmp
		// //later done with reduction
		// //same index as idx
		// //iterate over array and sum up values
		// for (int temp_index = 0; temp_index < (int)unique_blocks; temp_index++){
		// 	psnr_tmp += psnr_dlt_ray[temp_index];
		// 	switched += switched_ray[temp_index];
		// }
		/******************** Modified function!**************************/

#pragma omp parallel for
		for (int i = 0; i < (int)k; i++)
		{
			selected_blocks[i].update();
		}
		for (int i = 0; i < (int)k; i++)
		{
			if (selected_blocks[i].changedCenter)
			{
				changed_blocks[changed_blocks_size++] = i;
			}
		}
		// calculate PSNR
		{
			float psnr = (float)psnr_tmp;
			float a = quantizeMaximum(vol);
			float b = 0.0f;
			psnr /= (float)(volumeSize.width * volumeSize.height * volumeSize.depth);
			psnr = (a - b) * (a - b) / psnr;
			psnr = 10.0f * logf(psnr) / logf(10.0f);
			std::cout << "  PSNR: " << psnr << "db" << std::endl;
		}
		changed = changed_blocks_size;
		run++;

		std::cout << "  iteration " << run << ": " << changed << " blocks changed (" << switched << " switched owner)." << std::endl;
	}
	return;
}




template <> void vector_quant_gpu_outer_caller<unsigned short, unsigned long long, unsigned long long, unsigned short>
(unsigned short *vol, cudaExtent &volumeSize, std::vector<CenterBlock<unsigned long long, unsigned long long, unsigned short>>  &selected_blocks, std::vector<VolumeBlock<unsigned long long, unsigned short>> &blocks,int unique_blocks, bool bruteForce, unsigned int k){
	printf("Hi there");
	return;
}


template <> void vector_quant_gpu_outer_caller<ushort4, unsigned long long, ulonglong4, ushort4>
(ushort4 *vol, cudaExtent &volumeSize, std::vector<CenterBlock<unsigned long long, ulonglong4, ushort4>>&selected_blocks, std::vector<VolumeBlock<unsigned long long, ushort4>> &blocks, int unique_blocks, bool bruteForce, unsigned int k){
	printf("Hi there");
	return;
}


template <> void vector_quant_gpu_outer_caller<uchar4, unsigned long long, uint4, uchar4>
(uchar4 *vol, cudaExtent &volumeSize, std::vector<CenterBlock<unsigned long long, uint4, uchar4>> &selected_blocks, std::vector<VolumeBlock<unsigned long long, uchar4>> &blocks, int unique_blocks, bool bruteForce, unsigned int k){
	printf("Hi there");
	return;
}
