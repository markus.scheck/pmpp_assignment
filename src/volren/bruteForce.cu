#include "volumeReader.h"
#include <vector>
#include <chrono>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/extrema.h>
#include <cuda_runtime.h>


#define CUDA_CHECK_ERROR                                                       \
    do {                                                                       \
        const cudaError_t err = cudaGetLastError();                            \
        if (err != cudaSuccess) {                                              \
            const char *const err_str = cudaGetErrorString(err);               \
            std::cerr << "Cuda error in " << __FILE__ << ":" << __LINE__ - 1   \
                      << ": " << err_str << " (" << err << ")" << std::endl;   \
            exit(EXIT_FAILURE);                                                \
        }                                                                      \
    } while(0);

//Normally, this should be a CenterBlock<...> variable
//However, this variable couldn't be initialised
//This is the workaround with a precalculated byte length
//352 byte for: CenterBlock<unsigned long long, uint, unsigned char>
//672 byte for: CenterBlock<unsigned long long, unsigned long long, unsigned short>
#define GMEM_SIZE 672 //[352]
__constant__ char globCenter[GMEM_SIZE];

//*************************************************************************//
//**********************  Forward Declarations ****************************//

template <class A, class D, class O> 
void bruteForce_outer_caller(std::vector<CenterBlock<A, D, O>> &selected_blocks, std::vector<VolumeBlock<A, O>> &blocks, unsigned int k, unsigned int unique_blocks);

template <class A, class D, class O> 
void bruteForce_Search_CPU(std::vector<CenterBlock<A, D, O>> &selected_blocks, std::vector<VolumeBlock<A, O>> &blocks, unsigned int k, unsigned int unique_blocks);

template <class A, class D, class O> 
void bruteForce_Search_CPU_measure(std::vector<CenterBlock<A, D, O>> &selected_blocks, std::vector<VolumeBlock<A, O>> &blocks, unsigned int k, unsigned int unique_blocks);

//**********************  Forward Declarations ****************************//
//*************************************************************************//

template <class A, class O> 
__global__ void bf_centerDist_kernel( VolumeBlock<A, O> *vol_blocks, A* final_ray_raw, const bool eq, const unsigned int done, const unsigned int unique_blocks);


template <> __global__ void bf_centerDist_kernel(VolumeBlock<unsigned long long, uchar> *vol_blocks, unsigned long long* final_ray_raw, const bool eq,  const unsigned int done, const unsigned int unique_blocks){
    int tx = threadIdx.x + (blockIdx.x*blockDim.x);
    if(tx>=unique_blocks){
        return;
    }
    CenterBlock<unsigned long long, uint, uchar>* center2 = (CenterBlock<unsigned long long, uint, uchar>*) globCenter;
    CenterBlock<unsigned long long, uint, uchar> center = center2[0];
    bool temp_res; 
    VolumeBlock<unsigned long long, uchar> &blk = vol_blocks[tx];
    unsigned long long &min_dist = blk.lastDist;

    double zdist = center.zeroDist;
    double vdist = blk.zeroDist;


    if ((zdist - vdist)*(zdist - vdist) > min_dist){
        temp_res = false;
    }else{
        unsigned long long dd = 0;
        for (unsigned int i = 0; i < 64; i++)
        {   
            dd += (unsigned long long)((unsigned int)center.value[i] * (unsigned int)blk.value[i]);
        }
        unsigned long long d = center.squared + blk.squared;
        d -= dd;
        d -= dd;
        if ((d < min_dist) || ((d == min_dist) && eq))
        {
            min_dist = d;
            temp_res = true;
        }else{
            temp_res = false; 
        }
    }

    if (temp_res){
        blk.last = done;
    }  
    final_ray_raw[tx] = blk.lastDist;
}

template <> __global__ void bf_centerDist_kernel(VolumeBlock<unsigned long long, unsigned short> *vol_blocks, unsigned long long* final_ray_raw, const bool eq, const unsigned int done,const unsigned int unique_blocks){
    
    int tx = threadIdx.x + (blockIdx.x*blockDim.x);
    if(tx>=unique_blocks){
        return;
    }
    CenterBlock<unsigned long long, unsigned long long, unsigned short>* center2 = (CenterBlock<unsigned long long, unsigned long long, unsigned short>*) globCenter;
    CenterBlock<unsigned long long, unsigned long long, unsigned short> center = center2[0];
    bool temp_res; 
    VolumeBlock<unsigned long long, unsigned short> &blk = vol_blocks[tx];
    unsigned long long &min_dist = blk.lastDist;

    double zdist = center.zeroDist;
    double vdist = blk.zeroDist;

    if ((zdist - vdist)*(zdist - vdist) > min_dist){
        temp_res = false;
    }else{
        unsigned long long dd = 0;
        for (unsigned int i = 0; i < 64; i++)
        {   
            dd += (unsigned long long)((unsigned int)center.value[i] * (unsigned int)blk.value[i]);
        }
        unsigned long long d = center.squared + blk.squared;
        d -= dd;
        d -= dd;
        if ((d < min_dist) || ((d == min_dist) && eq))
        {
            min_dist = d;
            temp_res = true;
        }else{
            temp_res = false; 
        }
    }

    if (temp_res){
        blk.last = done;
    }  
    final_ray_raw[tx] = blk.lastDist;
}

template <> __global__ void bf_centerDist_kernel(VolumeBlock<unsigned long long, uchar4> *vol_blocks, unsigned long long* final_ray_raw, const bool eq, const unsigned int done, const unsigned int unique_blocks){
    //Nothing to see here...
    //Just a stub for the compiler
    return;
}

template <> __global__ void bf_centerDist_kernel(VolumeBlock<unsigned long long, ushort4> *vol_blocks, unsigned long long* final_ray_raw, const bool eq, const unsigned int done, const unsigned int unique_blocks){
    //Nothing to see here...
    //Just a stub for the compiler
    return;
}








template <class A, class D, class O> 
void bruteForce_Search_GPU(std::vector<CenterBlock<A, D, O>> &selected_blocks, std::vector<VolumeBlock<A, O>> &blocks, unsigned int k, unsigned int unique_blocks, bool reg_approach){

    //*************************************************************************//
    //*************************************************************************//
    //*********************** (Step 0:) Initialisation ************************//

	unsigned int *changed_blocks = new unsigned int[k];
	for (unsigned int i = 0; i < k; i++) changed_blocks[i] = i;
	unsigned int changed_blocks_size = k;
	unsigned int changed = changed_blocks_size;
    
    
    
    A global_min = A(0x7fffffffffffffffll);
    unsigned int min_idx = 0;

    //*********************** (Step 0:) Initialisation ************************//
    //*************************************************************************//
    //*************************************************************************//
    




    //*************************************************************************//
    //*************************************************************************//
    //**************** Step 1: Find codeword closest to origin ****************//

    A* dev_raw_ptr;

    auto start_findMinimumBlock = std::chrono::high_resolution_clock::now();
    A squared_ray[unique_blocks];

    unsigned int uniqueBlock_size = unique_blocks * sizeof(A);

    //We only need a single value from every block
    //Therefore we extract this to avoid to copy all blocks
    for (int i = 0; i < (int)unique_blocks; i++){
        squared_ray[i] = blocks[i].dist();
    }


    //malloc space for later CUDA arrays
    cudaMalloc((void **) &dev_raw_ptr, uniqueBlock_size);
    CUDA_CHECK_ERROR


    //copy first array to GPU
    cudaMemcpy(dev_raw_ptr , squared_ray, uniqueBlock_size, cudaMemcpyHostToDevice);
    CUDA_CHECK_ERROR

 
    
    thrust::device_ptr<A> squared_device = thrust::device_pointer_cast(dev_raw_ptr);
    thrust::device_ptr<A> min_ptr = thrust::min_element(squared_device, squared_device + unique_blocks);

    min_idx = &min_ptr[0] - &squared_device[0];
    global_min = min_ptr[0];


    //**************** Step 1: Find codeword closest to origin ****************//
    //*************************************************************************//
    //*************************************************************************//

    selected_blocks[0] = blocks[min_idx];
    int next_idx = 0;
    A global_max = A(0);


    //*************************************************************************//
    //*************************************************************************//
    //********* Step 2: Find Block with largest distance from center **********//

    A dist_ray[unique_blocks];
    A* dist_device_raw;

    CenterBlock<A, D, O> &center = selected_blocks[0];
    for (int i = 0; i < (int)unique_blocks; i++)
    {
        VolumeBlock<A, O> &blk = blocks[i];
        //We can not cutout the manipulation of the "blk" variable
        //Reason: Original "blocks" array is updated -> due to& Operator
        dist_ray[i] = blk.lastDist = center.dist(blk);
        blk.last = 0;
    }


    cudaMemcpy(dev_raw_ptr , dist_ray, uniqueBlock_size, cudaMemcpyHostToDevice);
    CUDA_CHECK_ERROR

    thrust::device_ptr<A> dist_device = thrust::device_pointer_cast(dev_raw_ptr);
    thrust::device_ptr<A> max_ptr = thrust::max_element(dist_device, dist_device + unique_blocks);

    next_idx = (&max_ptr[0] - &dist_device[0]);

    cudaFree(dev_raw_ptr);

    //********* Step 2: Find Block with largest distance from center **********//
    //*************************************************************************//
    //*************************************************************************//


    A final_ray[unique_blocks];
    A* final_ray_raw;


    //*************************************************************************//
    //*************************************************************************//
    //******************** Step 3: Update all Blocks **************************//

    cudaMalloc((void **) &final_ray_raw, uniqueBlock_size);
    CUDA_CHECK_ERROR


    if(reg_approach){

        unsigned int done = 1;
        while (done < k)
        {
            selected_blocks[done] = blocks[next_idx];
            // update distances
            global_max = A(0);
            next_idx = 0;
            CenterBlock<A, D, O> &center = selected_blocks[done];


#pragma omp parallel for
            for (int i = 0; i < (int)unique_blocks; i++)
            {
                
                VolumeBlock<A, O> &blk = blocks[i];
                if (center.dist(blk, blk.lastDist, false))
                {
                    blk.last = done;
                }
                final_ray[i] = blk.lastDist;
            }

            //malloc space for dist array
            cudaMemcpy(final_ray_raw , final_ray, uniqueBlock_size, cudaMemcpyHostToDevice);
            CUDA_CHECK_ERROR


            thrust::device_ptr<A> final_device = thrust::device_pointer_cast(final_ray_raw);
            thrust::device_ptr<A> max_ptr = thrust::max_element(final_device, final_device + unique_blocks);

            next_idx = (&max_ptr[0] - &final_device[0]);
            done++;

            }
    }else{
   //************ Step 3: Alternative approach for other datatypes************//
    //The specific approach is used for one specfic set of datatypes:
        //A = unsigned long long
        //D = uint
        //O = uchar
        //needed when matrix size exceeds block size
        const int max_threads_per_block = 1024;
        int num_blocks = 0;
        int cur_threads_per_block = 0;

        if(unique_blocks<=max_threads_per_block){
            cur_threads_per_block = unique_blocks;
            num_blocks = 1;

        }else{
            cur_threads_per_block = max_threads_per_block;
            num_blocks = unique_blocks / max_threads_per_block;

	        if (unique_blocks % max_threads_per_block){
                ++num_blocks;
            };
        };

        dim3 threadsPerBlock(cur_threads_per_block);
        dim3 numBlocks(num_blocks); 

        //normally, shared mem should not be needed
        size_t sharedMemBytes = 64; 

        //copy volume blocks to GPU
        VolumeBlock<A, O>* vol_blocks_dev;
        CenterBlock<A, D, O>* center_dev;
        bool* bool_res_dev;

        //convert volume block vector to normal array 
        VolumeBlock<A, O>* vol_ray = &blocks[0];

        //allocate space for the volume blocks
        int size_vol_blocks = unique_blocks*sizeof(VolumeBlock<A, O>);
        cudaMalloc((void **) &vol_blocks_dev, size_vol_blocks);
        CUDA_CHECK_ERROR

        //copy the volume blocks to the device
        cudaMemcpy(vol_blocks_dev , vol_ray, size_vol_blocks, cudaMemcpyHostToDevice);
        CUDA_CHECK_ERROR


        unsigned int done = 1;
        while (done < k){
            selected_blocks[done] = blocks[next_idx];
            // update distances
            global_max = A(0);
            next_idx = 0;

            //copy centerblock to global memory for faster parallel access
            cudaMemcpyToSymbol(globCenter, &(selected_blocks[done]), GMEM_SIZE);
            CUDA_CHECK_ERROR
            CenterBlock<A, D, O> &center = selected_blocks[done];
  
            bf_centerDist_kernel<A,O><<<numBlocks, threadsPerBlock>>>(vol_blocks_dev, final_ray_raw, false, done, unique_blocks);
            CUDA_CHECK_ERROR


            thrust::device_ptr<A> final_device = thrust::device_pointer_cast(final_ray_raw);
            thrust::device_ptr<A> max_ptr = thrust::max_element(final_device, final_device + unique_blocks);
            next_idx = (&max_ptr[0] - &final_device[0]);
            done++;
            }

            cudaMemcpy(vol_ray, vol_blocks_dev, size_vol_blocks, cudaMemcpyDeviceToHost);
            CUDA_CHECK_ERROR

    }
    cudaFree(final_ray_raw);
    //******************** Step 3: Update all Blocks **************************//
    //*************************************************************************//
    //*************************************************************************//
}






#define MODE_GPU 1
#define MODE_CPU 2
template <class A, class D, class O> 
void bruteForce_inner_caller(std::vector<CenterBlock<A, D, O>> &selected_blocks, std::vector<VolumeBlock<A, O>> &blocks, unsigned int k, unsigned int unique_blocks, bool reg_approach){
    //Wake up Cuda Memory manager if not done already
    //This allows consistent time measurements 
    cudaFree(0);

    //Mode = 1 GPU computation
    //Mode = 2 CPU computation without annotations (and OpenMP parallelized) - used for obtain timings
    //Mode = 3 CPU Computation without parallelism for debug purposes
    int mode = MODE_CPU;
    if(mode == 1){    
        auto time_GPU_start = std::chrono::high_resolution_clock::now(); 
        bruteForce_Search_GPU<A,D,O>(selected_blocks, blocks, k, unique_blocks, reg_approach);
            
        auto time_GPU_end = std::chrono::high_resolution_clock::now();
        double time_GPU = (double)std::chrono::duration_cast<std::chrono::nanoseconds>(time_GPU_end - time_GPU_start).count();
        std::cout << "GPU Time for full bruteforce search: ";
        std::cout << time_GPU << "ns = " << time_GPU / 1000000.0 << "s = " << time_GPU / 60000000.0 << "min" << std::endl<<std::endl;
    }
    else if(mode == 2){    
        auto time_CPU_start = std::chrono::high_resolution_clock::now(); 

        bruteForce_Search_CPU<A,D,O>(selected_blocks, blocks, k, unique_blocks);

        auto time_CPU_end = std::chrono::high_resolution_clock::now();
        double time_CPU = (double)std::chrono::duration_cast<std::chrono::nanoseconds>(time_CPU_end - time_CPU_start).count();
        std::cout << "\nCPU Time for full bruteforce search: ";
        std::cout << time_CPU << "ns = " << time_CPU / 1000000.0 << "s = " << time_CPU / 60000000.0 << "min" << std::endl<<std::endl;
    }
    return;
}



template <> void bruteForce_outer_caller<unsigned long long, uint, uchar>
(std::vector<CenterBlock<unsigned long long, uint, uchar>> &selected_blocks, std::vector<VolumeBlock<unsigned long long, uchar>> &blocks, unsigned int k, unsigned int unique_blocks)
{
    //set the rep_approach value to false
    //this combination of single-value types can uses the faster approach with our custom kernel
        //A = unsigned long long
        //D = uint
        //O = uchar
    bruteForce_inner_caller<unsigned long long, uint, uchar>(selected_blocks, blocks, k,unique_blocks, false);
    return;
};

template <> void bruteForce_outer_caller<unsigned long long, unsigned long long, unsigned short>
(std::vector<CenterBlock<unsigned long long, unsigned long long, unsigned short>> &selected_blocks, std::vector<VolumeBlock<unsigned long long, unsigned short>> &blocks, unsigned int k, unsigned int unique_blocks)
{
    //set the rep_approach value to false
    //this combination of single-value types can uses the faster approach with our custom kernel
        //A = unsigned long long
        //D = unsigned long long
        //O = unsigned short
    bruteForce_inner_caller<unsigned long long, unsigned long long, unsigned short>(selected_blocks, blocks, k,unique_blocks, false);
    return;
};

template <> void bruteForce_outer_caller<unsigned long long, uint4, uchar4>
(std::vector<CenterBlock<unsigned long long, uint4, uchar4>> &selected_blocks, std::vector<VolumeBlock<unsigned long long, uchar4>> &blocks, unsigned int k, unsigned int unique_blocks)
{
    //set the rep_approach value to true
    //We use the generic approach build with thrust
    bruteForce_inner_caller<unsigned long long, uint4, uchar4>(selected_blocks, blocks, k,unique_blocks, true);
    return;
};

template <> void bruteForce_outer_caller<unsigned long long, ulonglong4, ushort4>
(std::vector<CenterBlock<unsigned long long, ulonglong4, ushort4>> &selected_blocks, std::vector<VolumeBlock<unsigned long long, ushort4>> &blocks, unsigned int k, unsigned int unique_blocks)
{
    //set the rep_approach value to true
    //We use the generic approach build with thrust
    bruteForce_inner_caller<unsigned long long, ulonglong4, ushort4>(selected_blocks, blocks, k,unique_blocks, true);
    return;
};






/*Regular CPU implementation of function*/
/*Accelerated using Open MP*/
template <class A, class D, class O> 
void bruteForce_Search_CPU(std::vector<CenterBlock<A, D, O>> &selected_blocks, std::vector<VolumeBlock<A, O>> &blocks, unsigned int k, unsigned int unique_blocks){
A global_min = A(0x7fffffffffffffffll);
		unsigned int min_idx = 0;

#pragma omp parallel for
		for (int i = 0; i < (int)unique_blocks; i++)
		{
			A p;
			p = blocks[i].dist();// * blocks[i].count;
			if (p <= global_min)
			{
#pragma omp critical
				{
					if ((p < global_min) || ((p == global_min) && (i < (int)min_idx)))
					{
						global_min = p;
						min_idx = i;
					}
				}
			}
		}
		selected_blocks[0] = blocks[min_idx];
		int next_idx = 0;
		A global_max = A(0);
		// update distances
		{
			CenterBlock<A, D, O> &center = selected_blocks[0];
#pragma omp parallel for
			for (int i = 0; i < (int)unique_blocks; i++)
			{
				VolumeBlock<A, O> &blk = blocks[i];
				A p = blk.lastDist = center.dist(blk);
				blk.last = 0;
				if (p >= global_max)
				{
#pragma omp critical
					{
						if ((p > global_max) || ((p == global_max) && (i < (int)next_idx)))
						{
							global_max = p;
							next_idx = i;
						}
					}
				}
			}
		}
		unsigned int done = 1;
		while (done < k)
		{
			selected_blocks[done] = blocks[next_idx];
			// update distances
			global_max = A(0);
			next_idx = 0;
			CenterBlock<A, D, O> &center = selected_blocks[done];
#pragma omp parallel for
			for (int i = 0; i < (int)unique_blocks; i++)
			{
				VolumeBlock<A, O> &blk = blocks[i];
				if (center.dist(blk, blk.lastDist, false))
				{
					blk.last = done;
				}

				A p = blk.lastDist;


                double zdist = center.zeroDist;
                double vdist = blk.zeroDist;



				if ((done + 1 < k) && (p >= global_max))
				{
#pragma omp critical
					{
						if ((p > global_max) || ((p == global_max) && (i < (int)next_idx)))
						{
							global_max = p;
							next_idx = i;
						}
					}
				}
			}
			done++;
            if ((done * 80 / k) > ((done - 1) * 80 / k)) std::cout << ".";
		}
}