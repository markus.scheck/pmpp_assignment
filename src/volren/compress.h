template<typename T, typename T2> 
void cmpr_gpu(T **dst, size_t* dst_size, unsigned char* src, size_t src_size, cudaExtent volumeSize, int c, int mask);