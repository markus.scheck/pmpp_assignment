####################### Small Size -  32x32x32 ##############################
#../build/volren -volume=testing/small_size/Bucky-32x32x32_8.ddv -ddv file -8bit -compare=1 -bruteForce -compression=1  -lossy=1 -size=32 -export_version=4 -export=small_size/small_size_cur.raw
#../build/volren -volume=testing/small_size/Bucky-32x32x32_8.ddv -ddv file -8bit -compare=1 -bruteForce -compression=1  -lossy=1 -size=32 -export_version=4 -export=small_size/small_size_cur.ddv
# echo "$(tput setaf 1)Differences:$(tput sgr 0)"
# diff small_size/small_size_sol.raw small_size/small_size_cur.raw
# diff small_size/small_size_sol.ddv small_size/small_size_cur.ddv


# # ####################### Medium Size - 208x208x123 ###########################
../build/volren -volume=testing/medium_size/stagbeetle208x208x123.dat  -dat file -16bit -compare=1 -bruteForce -compression=1 -lossy=1 -export=medium_size/medium_size_cur.raw
# ../build/volren -volume=testing/medium_size/stagbeetle208x208x123.dat  -dat file -16bit -compare=1 -bruteForce -compression=1 -lossy=1 -export=medium_size/medium_size_cur.ddv
# echo "$(tput setaf 1)Differences:$(tput sgr 0)"
# diff medium_size/medium_size_sol.raw medium_size/medium_size_cur.raw


########################### Big Size - 512x499x512###########################
#Big Size - 512x499x512 - Takes forever to run 
#./build/volren -volume=christmastree-denoise-512x499x512_16.ddv -ddv file -16bit -compare=1 -bruteForce -compression=1 -export_version=4 -lossy=1 -xsize=512 -ysize=499 -zsize=512
